# Random User Project
This project is a part of the hiring process of Lydia Company. It aims to show a qucik overview of my Android stack. 

## Summary
The specifications requires to query, paginate and display data from [random user api](https://randomuser.me/api/1.0/?seed=lydia&results=10&page=1)

## Stack
As an Android modern development enthousiast, I choose to use its latest standards as much as possible. Which led to me to the following stack: 

- Kotlin Multiplatform
- SqlDelight
- Ktor Client
- Coroutines Flow
- Jetpack Compose
- Compose Paging
- Compose Navigation

## Installation
- use your favorite git commands to clone the project
- make sure you downloaded the latest Android Studio Canary Version
- Hit play or enter your favorite shortcut to run the project

## Issues
Following are identified issues that I will fix soon. Most of them are related to the fact that many of the stack's elements are not stable yet. So i will be tracking any update to reflect it in this project. 

- The used version of Compose Paging seems to be compatible only with Kotlin 1.4.10
- Which causes many problems since most of jetpack features targets the latest Kotlin version. 
- Storage issue with Paging preloaded data. May be related to SqlDelight
- The images retrieved by the api lacks quality
- add unit tests

## Demo
![demo gif](test.gif)





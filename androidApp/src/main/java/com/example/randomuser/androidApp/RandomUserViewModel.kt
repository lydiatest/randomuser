package com.example.randomuser.androidApp

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.example.randomuser.shared.model.domain.RandomUserModel
import com.example.randomuser.shared.repository.RandomUserRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RandomUserViewModel(
    private val randomUserRepository: RandomUserRepository
) : ViewModel() {

    val users: Flow<PagingData<RandomUserModel>> =
        Pager(PagingConfig(pageSize = Int.MAX_VALUE)) {
            RandomUserSource(randomUserRepository, this)
        }.flow
            .cachedIn(viewModelScope)

    // TODO find other way to collect state from Compose Paging
    val _stateFullUsers: MutableStateFlow<List<RandomUserModel>> = MutableStateFlow(emptyList())
    val stateFullUsers: StateFlow<List<RandomUserModel>> = _stateFullUsers

    init {
        viewModelScope.launch {
            randomUserRepository.fetchRandomUsersFlow()?.collect {
                _stateFullUsers.value = it
            }
        }
    }
}

class RandomUserSource(
    private val randomUserRepository: RandomUserRepository,
    private val viewModel: RandomUserViewModel
) : PagingSource<Int, RandomUserModel>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, RandomUserModel> {
        return try {
            val nextPage = params.key ?: 1
            val usersResponse = randomUserRepository.fetchUsers(nextPage)
              LoadResult.Page(
                data = usersResponse,
                prevKey = if (nextPage == 1) null else nextPage - 1,
                nextKey = nextPage + 1
            )
        } catch (e: Exception) {
            val cachedUsers = viewModel.stateFullUsers.value
            if (cachedUsers.isNullOrEmpty()) {
                LoadResult.Error(e)
            } else {
                LoadResult.Page(
                    data = cachedUsers,
                    prevKey = null,
                    nextKey = null
                )
            }
        }
    }
}

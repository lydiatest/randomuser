package com.example.randomuser.androidApp.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.randomuser.androidApp.R

@Composable
fun ErrorView(
    message: String,
    onClickRetry: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth().fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.padding(24.dp))
        Text(
            text = message,
            style = MaterialTheme.typography.h1,
            modifier = Modifier.padding(16.dp)
        )
        Button(onClick = onClickRetry) {
            Text(
                text = stringResource(R.string.retry_button),
                modifier = Modifier.padding(16.dp)
            )
        }
    }
}
package com.example.randomuser.androidApp.di

import com.example.randomuser.androidApp.RandomUserSource
import com.example.randomuser.androidApp.RandomUserViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appKoinModule = module {
    viewModel { RandomUserViewModel(get()) }
    single { RandomUserSource(get(), get()) }
}
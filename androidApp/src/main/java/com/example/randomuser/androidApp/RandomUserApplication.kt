package com.example.randomuser.androidApp

import android.app.Application
import com.example.randomuser.androidApp.di.appKoinModule
import com.example.randomuser.shared.di.initKoin
import com.example.randomuser.shared.repository.appContext
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.component.KoinComponent

class RandomUserApplication: Application(), KoinComponent {

    override fun onCreate() {
        super.onCreate()

        appContext = this

        initKoin {
            androidLogger()
            androidContext(this@RandomUserApplication)
            modules(appKoinModule)
        }
    }
}
package com.example.randomuser.androidApp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.setContent
import androidx.navigation.NavType
import androidx.navigation.compose.*
import com.example.randomuser.androidApp.theme.RandomUserTheme
import com.example.randomuser.androidApp.ui.UserDetails
import com.example.randomuser.androidApp.ui.UsersList
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val randomUserViewModel: RandomUserViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RandomUserTheme {
                MainView(viewModel = randomUserViewModel)
            }
        }
    }
}

@Composable
fun MainView(viewModel: RandomUserViewModel) {

    val navController = rememberNavController()

    Scaffold {
        NavHost(navController = navController, startDestination = "home") {
            composable("home") {
                UsersList(
                    users = viewModel.users,
                    userSelected = { navController.navigate("details/${it}") })
            }

            composable(
                "details/{name}",
                arguments = listOf(navArgument("name") { type = NavType.StringType })
            ) { backStackEntry ->
                val args = backStackEntry.arguments?.getString("name") as String
                UserDetails(
                    user = viewModel.stateFullUsers.value.firstOrNull { it.fullName == args }
                )
            }
        }
    }
}

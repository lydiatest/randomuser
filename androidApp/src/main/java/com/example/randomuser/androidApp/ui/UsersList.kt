package com.example.randomuser.androidApp.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import com.example.randomuser.shared.model.domain.RandomUserModel
import dev.chrisbanes.accompanist.coil.CoilImage
import kotlinx.coroutines.flow.Flow

@Composable
fun UsersList(
    users: Flow<PagingData<RandomUserModel>>,
    userSelected: (String) -> Unit
) {
    Column {

        HomeHeader()

        val lazyUsers = users.collectAsLazyPagingItems()

        LazyColumn {

            items(lazyUsers) { user ->
                user?.let { UserItem(it, userSelected) }
            }

            lazyUsers.apply {
                when {
                    loadState.refresh is LoadState.Loading -> {
                        item { LoadingView(modifier = Modifier.fillParentMaxSize()) }
                    }
                    loadState.append is LoadState.Loading -> {
                        item { LoadingItem() }
                    }
                    loadState.refresh is LoadState.Error -> {
                        item {
                            ErrorView(
                                message = "Failed to fetch random users",
                                onClickRetry = {
                                    retry()
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun UserItem(
    user: RandomUserModel,
    userSelected: (String) -> Unit
) {
    Card(
        elevation = 12.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(12.dp)
            .clickable(onClick = { userSelected(user.fullName) })
    ) {
        Row {
            CoilImage(
                data = user.mediumPictureUrl,
                contentScale = ContentScale.Crop,
                fadeIn = true,
                modifier = Modifier
                    .size(80.dp)
                    .padding(12.dp)
                    .clip(CircleShape)
            )
            Spacer(modifier = Modifier.size(16.dp))
            Column(modifier = Modifier.padding(top = 12.dp)) {
                Text(text = user.fullName, style = MaterialTheme.typography.body1)
                Text(text = user.email, style = MaterialTheme.typography.body2)
            }
        }
    }
}

@Composable
fun HomeHeader(
) {
    Card(
        elevation = 24.dp,
        modifier = Modifier.fillMaxWidth()
    ) {
        Column {
            Spacer(modifier = Modifier.padding(12.dp))
            Text(
                text = "Random User",
                modifier = Modifier.padding(start = 24.dp),
                style = MaterialTheme.typography.h1
            )
            Spacer(modifier = Modifier.padding(12.dp))
        }
    }
}
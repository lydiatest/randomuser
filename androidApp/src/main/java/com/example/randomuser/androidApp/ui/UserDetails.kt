package com.example.randomuser.androidApp.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import com.example.randomuser.shared.model.domain.RandomUserModel
import dev.chrisbanes.accompanist.coil.CoilImage

@Composable
fun UserDetails(
    user: RandomUserModel?
) {
    user?.let {
        LazyColumn {
            item {
                CoilImage(
                    data = it.largePictureUrl,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxWidth()
                )
            }
            item {
                Text(
                    text = it.fullName,
                    modifier = Modifier.padding(top = 16.dp, start = 12.dp),
                    style = MaterialTheme.typography.body1
                )
            }
            item {
                Text(
                    text = it.email,
                    modifier = Modifier.padding(top = 12.dp, start = 12.dp),
                    style = MaterialTheme.typography.body1
                )
            }
            item {
                Text(
                    text = it.location,
                    modifier = Modifier.padding(top = 12.dp, start = 12.dp),
                    style = MaterialTheme.typography.body1
                )
            }
            item {
                Text(
                    text = it.phoneNumber,
                    modifier = Modifier.padding(top = 12.dp, start = 12.dp),
                    style = MaterialTheme.typography.body1
                )
            }
            item {
                Text(
                    text = it.cellNumber,
                    modifier = Modifier.padding(top = 12.dp, start = 12.dp),
                    style = MaterialTheme.typography.body1
                )
            }
            item {
                Text(
                    text = it.gender,
                    modifier = Modifier.padding(top = 12.dp, start = 12.dp),
                    style = MaterialTheme.typography.body1
                )
            }
        }
    }
}
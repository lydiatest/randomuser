package com.example.randomuser.shared.model.raw

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
data class Location(
    val city: String,
    val postcode: String,
    val state: String,
    val street: String
)

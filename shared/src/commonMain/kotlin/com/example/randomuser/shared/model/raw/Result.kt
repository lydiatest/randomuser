package com.example.randomuser.shared.model.raw

import kotlinx.serialization.Serializable

@Serializable
data class Result(
    val cell: String,
    val dob: Int,
    val email: String,
    val gender: String,
    val id: Id,
    val location: Location,
    val login: Login,
    val name: Name,
    val nat: String,
    val phone: String,
    val picture: Picture,
    val registered: Int
)
package com.example.randomuser.shared.model.raw

import kotlinx.serialization.Serializable

@Serializable
data class Id(
    val name: String,
    val value: String? = null
)
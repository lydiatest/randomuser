package com.example.randomuser.shared.repository

import com.example.randomuser.db.RandomUserModelDb
import com.example.randomuser.shared.api.RandomUserApi
import com.example.randomuser.shared.model.domain.RandomUserModel
import com.example.randomuser.shared.model.raw.GetUsersResponse
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject


class RandomUserRepository : KoinComponent {

    private val randomUserApi: RandomUserApi by inject()

    private val database = createDb()
    private val queries = database?.randomUserQueries

    suspend fun fetchUsers(currentPage: Int): List<RandomUserModel> {
        val users = randomUserApi.fetchUsers(currentPage)
        insertRandomUsers(users.toDomainModel())
        return users.toDomainModel()
    }

    fun insertRandomUsers(users : List<RandomUserModel>) {
        queries?.transaction {
            users.map {
                queries.insertItem(
                    fullName = it.fullName,
                    email = it.email,
                    location = it.location,
                    mediumPictureUrl = it.mediumPictureUrl,
                    largePictureUrl = it.largePictureUrl,
                    gender = it.gender,
                    phoneNumber = it.phoneNumber,
                    cellNUmber = it.cellNumber,
                    id= it.id
                )
            }
        }
    }

    fun fetchRandomUsers(): List<RandomUserModel>? {
        val users: List<RandomUserModelDb>? = queries?.transactionWithResult { queries.selectAll().executeAsList() }
        return users?.toDomainModel()
    }

    fun fetchRandomUsersFlow(): Flow<List<RandomUserModel>>? {
        return  queries?.selectAll()?.asFlow()?.mapToList()?.map { it.toDomainModel() }
    }

    fun wipeDatabase() {
        queries?.deleteAll()
    }

    private fun GetUsersResponse.toDomainModel() =
        this.results.map {
            RandomUserModel(
                fullName = "${it.name.first} ${it.name.last}",
                email = it.email,
                location = "${it.location.street}, ${it.location.postcode} ${it.location.city}, ${it.location.state}",
                mediumPictureUrl = it.picture.medium,
                largePictureUrl = it.picture.large,
                gender = it.gender,
                phoneNumber = it.phone,
                cellNumber = it.cell
            )
        }.toList()

    private fun List<RandomUserModelDb>.toDomainModel() =
        this.map {
            RandomUserModel(
                fullName = it.fullName,
                email = it.email,
                location = it.location,
                mediumPictureUrl = it.mediumPictureUrl,
                largePictureUrl = it.largePictureUrl,
                gender = it.gender,
                phoneNumber = it.phoneNumber,
                cellNumber = it.cellNUmber
            )
        }.toList()
}
package com.example.randomuser.shared.model.raw

import kotlinx.serialization.Serializable


@Serializable
data class GetUsersResponse(
    val info: Info,
    val results: List<Result>
)
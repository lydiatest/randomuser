package com.example.randomuser.shared.api

import com.example.randomuser.shared.model.raw.GetUsersResponse
import io.ktor.client.*
import io.ktor.client.request.*

class RandomUserApi(
    private val client: HttpClient,
    private val baseUrl: String = "https://randomuser.me/api/1.0/?seed=lydia&results=10"
) {
    suspend fun fetchUsers(currentPage: Int) = client.get<GetUsersResponse>("${baseUrl}&page=${currentPage}")
}
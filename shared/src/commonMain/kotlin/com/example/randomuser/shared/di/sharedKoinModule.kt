package com.example.randomuser.shared.di

import co.touchlab.kermit.Kermit
import com.example.randomuser.shared.api.RandomUserApi
import com.example.randomuser.shared.repository.RandomUserRepository
import com.example.randomuser.shared.repository.getLogger
import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import kotlinx.serialization.json.Json
import org.koin.core.context.startKoin
import org.koin.dsl.KoinAppDeclaration
import org.koin.dsl.module

fun initKoin(enableNetworkLogs: Boolean = false, appDeclaration: KoinAppDeclaration = {}) =
    startKoin {
        appDeclaration()
        modules(commonModule(enableNetworkLogs = enableNetworkLogs))
    }

fun initKoin() = initKoin(enableNetworkLogs = false)

fun commonModule(enableNetworkLogs: Boolean) = module {
    single { createJson() }
    single { createHttpClient(get(), enableNetworkLogs = enableNetworkLogs) }
    single { RandomUserRepository() }
    single { RandomUserApi(get()) }
    single { Kermit(getLogger()) }
}

fun createJson() = Json { isLenient = true; ignoreUnknownKeys = true }

fun createHttpClient(json: Json, enableNetworkLogs: Boolean) =  HttpClient {
    install(JsonFeature) {
        serializer = KotlinxSerializer(json)
    }
    if(enableNetworkLogs) {
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.INFO
        }
    }
}
package com.example.randomuser.shared.model.domain
data class RandomUserModel(
    val fullName: String,
    val email: String,
    val location: String,
    val id: String? = null,
    val mediumPictureUrl: String,
    val largePictureUrl: String,
    val gender: String,
    val phoneNumber: String,
    val cellNumber: String
)

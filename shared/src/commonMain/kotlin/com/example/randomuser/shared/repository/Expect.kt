package com.example.randomuser.shared.repository

import co.touchlab.kermit.Logger
import com.example.randomuser.db.RandomUserDatabase

expect fun getLogger(): Logger

expect fun createDb(): RandomUserDatabase?
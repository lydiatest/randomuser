package com.example.randomuser.shared.repository

import co.touchlab.kermit.Logger
import co.touchlab.kermit.PlatformThrowableStringProvider
import co.touchlab.kermit.Severity
import com.example.randomuser.db.RandomUserDatabase
import platform.Foundation.NSLog

actual fun createDb(): RandomUserDatabase? = null

actual fun getLogger(): Logger = object : Logger() {
    // TODO: Why cant I import NSLogLogger?
    override fun log(severity: Severity, message: String, tag: String, throwable: Throwable?) {
        NSLog("%s: (%s) %s", severity.name, tag, message)
        throwable?.let {
            val string = PlatformThrowableStringProvider().getThrowableString(it)
            NSLog("%s", string)
        }
    }
}

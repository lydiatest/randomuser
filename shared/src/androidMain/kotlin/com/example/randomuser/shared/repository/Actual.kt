package com.example.randomuser.shared.repository

import android.content.Context
import co.touchlab.kermit.LogcatLogger
import co.touchlab.kermit.Logger
import com.example.randomuser.db.RandomUserDatabase
import com.squareup.sqldelight.android.AndroidSqliteDriver

lateinit var appContext: Context

actual fun createDb(): RandomUserDatabase? {
    val driver = AndroidSqliteDriver(RandomUserDatabase.Schema, appContext, "randomuserdb.db")
    return RandomUserDatabase(driver)
}

actual fun getLogger(): Logger = LogcatLogger()

